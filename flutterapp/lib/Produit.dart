import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutterapp/pages/Affichage.dart';

class Produit {
  static String baseUrl = "http://localhost:8000";

  static Future<List> getAllProduit() async {
    try {
      var res = await http.get(Uri.parse(baseUrl + '/produits'));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future<List> getAllProduitByPrix() async {
    try {
      var res = await http.get(Uri.parse(baseUrl + '/produits/orderByPrix'));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future<List> getAllProduitByNom() async {
    try {
      var res = await http.get(Uri.parse(baseUrl + '/produits/orderByNom'));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static ajout(BuildContext context, title, body) async {
    try {
      Map<String, dynamic> data = {"title": title, "body": body};
      var res = await http.post(Uri.parse(baseUrl + '/posts'), body: data);
      if (res.statusCode == 201) {
        Navigator.pushNamed(context, '/liste', arguments: res.body);
      } else {
        Navigator.pushNamed(context, '/');
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future postProduit(String reference, String nomProduit,
      String description, double prix, int quantite) async {
    try {
      Map<String, dynamic> data = {
        "reference": reference,
        "nomProduit": nomProduit,
        "description": description,
        "prix": prix,
        "quantite": quantite
      };
      var res = await http.post(Uri.parse(baseUrl + '/produit/add'),
          headers: {"Content-Type": "application/json"},
          body: json.encode(data));
      if (res.statusCode == 200) {
        return res.body;
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future updateProduit(String reference, String nomProduit,
      String description, double prix, int quantite) async {
    try {
      var res = await http.put(Uri.parse(baseUrl + '/produit/$reference'),
          body: jsonEncode({
            "reference": reference,
            "nomProduit": nomProduit,
            "description": description,
            "prix": prix.toString(),
            "quantite": quantite.toString()
          }),
          headers: {"Content-Type": "application/json"});

      if (res.statusCode == 200) {
        return res.body;
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future deleteProduit(BuildContext context, String reference) async {
    try {
      var res = await http.delete(Uri.parse(baseUrl + '/produit/$reference'));
      if (res.statusCode == 200) {
        var produit = jsonDecode(res.body);
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("${produit['nomProduit']} supprimé")));

        // On recharge la liste des produits
        Navigator.pushNamedAndRemoveUntil(
            context, '/liste', ModalRoute.withName('/'));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Erreur lors de la suppression")));
      }
    } catch (err) {
      return Future.error(err);
    }
  }
}
