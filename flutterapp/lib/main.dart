import 'package:flutter/material.dart';
import 'package:flutterapp/pages/Affichage.dart';
import 'package:flutterapp/pages/LoginPage.dart';
import 'package:flutterapp/pages/AccessDenied.dart';
import 'package:flutterapp/pages/ListeUsers.dart';

void main() {
   WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // Ajouter cette ligne
      title: 'Administration M2L',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      routes: {
        '/': (context) => LoginPage(),
        '/liste': (context) => Affichage(),
        '/accessDenied': (context) => AccessDenied(),
        '/listeusers': (context) => ListeUsers(),
      },
    );
  }
}
