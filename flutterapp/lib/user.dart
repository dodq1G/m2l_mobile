class User {
  static late Map<String, dynamic> _userData = {};

  static void setUserData(Map<String, dynamic>? data) {
    _userData = data ?? {};
  }

  static Map<String, dynamic> getUserData() {
    return _userData;
  }
}
