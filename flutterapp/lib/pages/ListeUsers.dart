import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ListeUsers extends StatefulWidget {
const ListeUsers({Key? key}) : super(key: key);

@override
State<ListeUsers> createState() => _ListeUsersState();
}

class _ListeUsersState extends State<ListeUsers> {
late Future<List<dynamic>> _userList = Future.value([]);
final _nomUtilisateurController = TextEditingController();
final _mailController = TextEditingController();
final _mdpController = TextEditingController();
final _roleController = TextEditingController();
final _prenomUtilisateurController = TextEditingController();
int _selectedUserId = 0;

@override
void initState() {
  super.initState();
  _loadUsers();
}

Future<void> _loadUsers() async {
  try {
    _userList = getAllUsers();
    setState(() {});
  } catch (err) {
    return Future.error("erreur serveur");
  }
}

static Future<List> getAllUsers() async {
  try {
    const String baseUrl = 'http://localhost:8000';
    var res = await http.get(Uri.parse(baseUrl + '/user'));
    if (res.statusCode == 200) {
      return jsonDecode(res.body);
    } else {
      return Future.error("erreur serveur");
    }
  } catch (err) {
    return Future.error(err);
  }
}

static Future postUser(String nomUtilisateur, String mdp, String role,
    String mail, String prenomUtilisateur) async {
  try {
    Map<String, dynamic> data = {
      "nomUtilisateur": nomUtilisateur,
      "mdp": mdp,
      "role": role,
      "mail": mail,
      "prenomUtilisateur": prenomUtilisateur
    };
    const String baseUrl = 'http://localhost:8000';
    var res = await http.post(Uri.parse(baseUrl + '/user/new'),
        headers: {"Content-Type": "application/json"},
        body: json.encode(data));
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return Future.error("erreur serveur");
    }
  } catch (err) {
    return Future.error(err);
  }
}

static Future<void> updateUser(
  int id,
  String nomUtilisateur,
  String mail,
  String mdp,
  String role,
) async {
  const String baseUrl = 'http://localhost:8000';
  final response = await http.put(
    Uri.parse(baseUrl + '/user/$id'),
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode({
      'nomUtilisateur': nomUtilisateur,
      'mail': mail,
      'mdp': mdp,
      'role': role,
    }),
  );

  if (response.statusCode != 200) {
    throw Exception(response.body);
  }
}

Future<Object> deleteUser(String id) async {
  try {
    const String baseUrl = 'http://localhost:8000';
    var res = await http.delete(Uri.parse(baseUrl + '/user/$id'));
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return Future.error("erreur serveur");
    }
  } catch (err) {
    return Future.error(err);
  }
}

void _ouvrirPopupModifier(dynamic user) {
  _selectedUserId = user['id'];
  _nomUtilisateurController.text = user['nomUtilisateur'];
  _mailController.text = user['mail'];
  _mdpController.text = user['mdp'];
  _roleController.text = user['role'];

  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text("Modifier l'utilisateur"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _nomUtilisateurController,
              decoration: const InputDecoration(
                labelText: "Nom d'utilisateur",
              ),
            ),
            TextField(
              controller: _mailController,
              decoration: const InputDecoration(
                labelText: "Adresse e-mail",
              ),
            ),
            TextField(
              controller: _mdpController,
              obscureText: true,
              decoration: const InputDecoration(
                labelText: "Mot de passe",
              ),
            ),
            TextField(
              controller: _roleController,
              decoration: const InputDecoration(
                labelText: "Rôle",
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            child: const Text("Annuler"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          ElevatedButton(
            child: const Text("Modifier"),
            onPressed: () {
              updateUser(_selectedUserId, _nomUtilisateurController.text, _mailController.text, _mdpController.text, _roleController.text);
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}

@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: const Text("Liste des utilisateurs"),
      actions: [
        IconButton(
          icon: const Icon(Icons.refresh),
          onPressed: _loadUsers,
        ),
      ],
    ),
    body: Container(
      child: FutureBuilder<List<dynamic>>(
        future: _userList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (context, i) {
                final user = snapshot.data![i];
                return ListTile(
                  title: Text(user['nomUtilisateur']),
                  subtitle: Text(user['mail']),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.edit),
                        onPressed: () {
                          _ouvrirPopupModifier(user);
                        },
                      ),
                      IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: const Text(
                                  "Supprimer l'utilisateur",
                                ),
                                content: const Text(
                                    "Êtes-vous sûr de vouloir supprimer cet utilisateur ?"),
                                actions: [
                                  TextButton(
                                    child: const Text("Annuler"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  ElevatedButton(
                                    child: const Text("Supprimer"),
                                    onPressed: () async {
                                      Navigator.pop(
                                          context); // Fermer la popup
                                      try {
                                        await deleteUser(
                                            user['id'].toString());

                                        _loadUsers();
                                      } catch (err) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            content: Text(err.toString()),
                                            duration:
                                                const Duration(seconds: 5),
                                            action: SnackBarAction(
                                              label: 'OK',
                                              onPressed: () {},
                                            ),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                    ],
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    ),
floatingActionButton: FloatingActionButton(
  child: const Icon(Icons.add),
  onPressed: () {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Ajouter un utilisateur"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: _nomUtilisateurController,
                decoration: const InputDecoration(
                  labelText: "Nom d'utilisateur",
                ),
              ),
              TextField(
                controller: _mailController,
                decoration: const InputDecoration(
                  labelText: "Adresse e-mail",
                ),
              ),
              TextField(
                controller: _mdpController,
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: "Mot de passe",
                ),
              ),
              TextField(
                controller: _roleController,
                decoration: const InputDecoration(
                  labelText: "Rôle",
                ),
              ),
            ],
          ),
          actions: [
            TextButton(
              child: const Text("Annuler"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            ElevatedButton(
              child: const Text("Ajouter"),
              onPressed: () async {
                try {
                  await postUser(
                    _nomUtilisateurController.text,
                    _mdpController.text,
                    _roleController.text,
                    _mailController.text,
                    _prenomUtilisateurController.text,
                  );
                  Navigator.pop(context);
                  _loadUsers();
                } catch (err) {
                  // Gérer l'erreur ici
                }
              },
            ),
          ],
        );
      },
    );
  },
),

  );
}
}
