import 'package:flutter/material.dart';

class AccessDenied extends StatelessWidget {
  const AccessDenied({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Accès refusé"),
      ),
      body: Center(
        child: Text(
          "Vous n'avez pas le droit d'accéder à cette page.",
          style: Theme.of(context).textTheme.headline6,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
