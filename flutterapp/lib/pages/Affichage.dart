import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutterapp/Produit.dart';
import 'package:flutterapp/User.dart';
import 'package:flutterapp/pages/ListeUsers.dart';

class Affichage extends StatefulWidget {
  const Affichage({Key? key}) : super(key: key);

  @override
  State<Affichage> createState() => _AffichageState();
}

class _AffichageState extends State<Affichage> {
  late Future<List<dynamic>> _produitList = Future.value([]);
  late Map<String, dynamic>? _userData;

  @override
  void initState() {
    super.initState();
    _loadProduits();
    _loadUserData();
  }

  Future<void> _loadProduits() async {
    try {
      final produits = await Produit.getAllProduit();
      setState(() {
        _produitList = Future<List<dynamic>>.value(produits);
      });
    } catch (err) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(err.toString()),
          duration: const Duration(seconds: 5),
          action: SnackBarAction(
            label: 'OK',
            onPressed: () {},
          ),
        ),
      );
    }
  }

  Future<void> _loadProduitsByPrix() async {
    try {
      final produits = await Produit.getAllProduitByPrix();
      setState(() {
        _produitList = Future<List<dynamic>>.value(produits);
      });
    } catch (err) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(err.toString()),
          duration: const Duration(seconds: 5),
          action: SnackBarAction(
            label: 'OK',
            onPressed: () {},
          ),
        ),
      );
    }
  }

  Future<void> _loadProduitsByNom() async {
    try {
      final produits = await Produit.getAllProduitByNom();
      setState(() {
        _produitList = Future<List<dynamic>>.value(produits);
      });
    } catch (err) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(err.toString()),
          duration: const Duration(seconds: 5),
          action: SnackBarAction(
            label: 'OK',
            onPressed: () {},
          ),
        ),
      );
    }
  }

  Future<void> _loadUserData() async {
    _userData = await User.getUserData();
    setState(() {});
  }

  Future<void> _showAjouterProduitDialog() async {
    var referenceController = TextEditingController();
    var nomProduitController = TextEditingController();
    var descriptionController = TextEditingController();
    var prixController = TextEditingController();
    var quantiteController = TextEditingController();

    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Ajouter un produit'),
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                TextField(
                  controller: referenceController,
                  decoration: const InputDecoration(labelText: 'Référence'),
                ),
                TextField(
                  controller: nomProduitController,
                  decoration:
                      const InputDecoration(labelText: 'Nom du produit'),
                ),
                TextField(
                  controller: descriptionController,
                  decoration: const InputDecoration(labelText: 'Description'),
                ),
                TextField(
                  controller: prixController,
                  decoration: const InputDecoration(labelText: 'Prix'),
                ),
                TextField(
                  controller: quantiteController,
                  decoration: const InputDecoration(labelText: 'Quantité'),
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
              child: const Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Enregistrer'),
              onPressed: () async {
                try {
                  String reference = referenceController.text;
                  String nomProduit = nomProduitController.text;
                  String description = descriptionController.text;
                  double prix = 0;
                  int quantite = 0;

                  if (prixController.text.isNotEmpty) {
                    prix = double.parse(prixController.text);
                  }

                  if (quantiteController.text.isNotEmpty) {
                    quantite = int.parse(quantiteController.text);
                  }

                  await Produit.postProduit(
                    reference,
                    nomProduit,
                    description,
                    prix,
                    quantite,
                  );
                  Navigator.of(context).pop();
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Produit ajouté avec succès'),
                      duration: Duration(seconds: 2),
                    ),
                  );
                  setState(() {
                    _loadProduits();
                  });
                } catch (err) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Erreur: ${err.toString()}'),
                      duration: Duration(seconds: 5),
                      action: SnackBarAction(
                        label: 'OK',
                        onPressed: () {},
                      ),
                    ),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showDialog(dynamic produit) async {
    var referenceController = TextEditingController(text: produit['reference']);
    var nomProduitController =
        TextEditingController(text: produit['nomProduit']);
    var descriptionController =
        TextEditingController(text: produit['description']);
    var prixController =
        TextEditingController(text: produit['prix'].toString());
    var quantiteController =
        TextEditingController(text: produit['quantite'].toString());

    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Modifier le produit'),
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                TextField(
                  controller: referenceController,
                  decoration: const InputDecoration(labelText: 'Référence'),
                ),
                TextField(
                  controller: nomProduitController,
                  decoration:
                      const InputDecoration(labelText: 'Nom du produit'),
                ),
                TextField(
                  controller: descriptionController,
                  decoration: const InputDecoration(labelText: 'Description'),
                ),
                TextField(
                  controller: prixController,
                  decoration: const InputDecoration(labelText: 'Prix'),
                ),
                TextField(
                  controller: quantiteController,
                  decoration: const InputDecoration(labelText: 'Quantité'),
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
              child: const Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Enregistrer'),
              onPressed: () async {
                try {
                  String reference = referenceController.text;
                  String nomProduit = nomProduitController.text;
                  String description = descriptionController.text;
                  double prix = 0;
                  int quantite = 0;

                  if (prixController.text.isNotEmpty) {
                    prix = double.parse(prixController.text);
                  }

                  if (quantiteController.text.isNotEmpty) {
                    quantite = int.parse(quantiteController.text);
                  }

                  await Produit.updateProduit(
                    reference,
                    nomProduit,
                    description,
                    prix,
                    quantite,
                  );
                  Navigator.of(context).pop();
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Produit modifié avec succès'),
                      duration: Duration(seconds: 2),
                    ),
                  );
                  setState(() {
                    _loadProduits();
                  });
                } catch (err) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Erreur: ${err.toString()}'),
                      duration: Duration(seconds: 5),
                      action: SnackBarAction(
                        label: 'OK',
                        onPressed: () {},
                      ),
                    ),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _delete(BuildContext context, dynamic produit) async {
    await Produit.deleteProduit(context, produit['reference']);
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Produit supprimé avec succès'),
        duration: Duration(seconds: 2),
      ),
    );
    setState(() {
      _loadProduits();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Liste des produits'),
        actions: [
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            Padding(
                padding: EdgeInsets.all(2.0),
                child: FloatingActionButton(
                  child: Text('€ ↓'),
                  elevation: 0,
                  onPressed: () {
                    setState(() {
                      _loadProduitsByPrix();
                    });
                  },
                )),
            Padding(
                padding: EdgeInsets.all(2.0),
                child: FloatingActionButton(
                  child: Text('A-Z ↓'),
                  elevation: 0,
                  onPressed: () {
                    setState(() {
                      _loadProduitsByNom();
                    });
                  },
                ))
          ]),
          IconButton(
            icon: const Icon(Icons.person),
            tooltip: 'Utilisateurs',
            onPressed: _userData != null && _userData!['role'] == 'admin'
                ? () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ListeUsers()),
                    );
                  }
                : null,
          ),
        ],
      ),
      body: FutureBuilder<List<dynamic>>(
        future: _produitList,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text('Erreur: ${snapshot.error}'));
          }
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }
          final produits = snapshot.data!;
          return ListView.builder(
            itemCount: produits.length,
            itemBuilder: (BuildContext context, int index) {
              final produit = produits[index];
              return Card(
                child: ListTile(
                  title: Text(produit['nomProduit']),
                  subtitle:
                      Text('${produit['description']} \n${produit['prix']} €'),
                  trailing: Wrap(
                    spacing: 12,
                    children: <Widget>[
                      IconButton(
                        icon: const Icon(Icons.edit),
                        tooltip: 'Modifier',
                        onPressed: () {
                          _showDialog(produit);
                        },
                      ),
                      IconButton(
                        icon: const Icon(Icons.delete),
                        tooltip: 'Supprimer',
                        onPressed: () {
                          _delete(context, produit);
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton:
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            _showAjouterProduitDialog();
          },
        ),
      ]),
    );
  }
}
