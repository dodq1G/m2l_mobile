# M2L Mobile

Ce projet est une application mobile pour la Maison des Ligues de Lorraine (M2L), développée en Flutter.

## Installation

1. Cloner le repository :

```bash
git clone https://gitlab.com/dodq1G/m2l_mobile.git
```

2. Installer les dépendances :

```bash
cd m2l_mobile\flutterapp\
flutter pub get
```

3. Démarrer l'application :

```bash
flutter run
```

## Utilisation

L'application est accessible sur votre appareil mobile Android ou iOS. Vous pouvez naviguer sur l'application pour découvrir ses fonctionnalités. Elle est utilisable uniquement par les administrateurs et les administrateurs produits.

## Fonctionnalités

Voici les fonctionnalités disponibles dans l'application :

- Consulter la liste des produits
- Créer un nouveau produit
- Modifier un produit
- Supprimer un produit
- Consulter la liste des utilisateurs
- Modifier un utilisateur
- Supprimer un utilisateur

## Contribution

Les contributions sont les bienvenues ! Si vous souhaitez contribuer à ce projet, voici les étapes à suivre :

1. Forker le repository.
2. Créer une branche pour votre contribution :
```bash
   git checkout main
   ```
3. Faire vos modifications et tester l'application localement.
4. Ajouter et commiter les modifications :
```bash
   git add .
   git commit -m "Description de votre contribution"
   ```
5. Pousser les modifications sur votre fork :
```bash
   git push origin main
   ```
6. Créer une pull request sur le repository original.
